package jnyqide;


import javax.swing.UIManager;
import java.awt.*;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;


/**
 * @author unascribed
 * @version 1.01
 * 
 * Revised by Zeyu Jan-3-2013
 */

public class Main {
    boolean packFrame = false;

    // Construct the application
    public Main(String[] args) {
        String osName = System.getProperty("os.name");
        System.out.println(osName);
        Locale loc = Locale.getDefault();
        System.out.println("lLocale is " + loc.toString());
        if (osName.startsWith("Linux")) {
            // motif style has some extra buttons to iconify internal frames
            // but this obscures windows, and metal looks better anyway
            try {
                UIManager.setLookAndFeel(
                        "javax.swing.plaf.metal.MetalLookAndFeel");
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        // enable anti-aliasing -- not sure of the "right" way...
        System.setProperty("awt.useSystemAAFontSettings","on");
        System.setProperty("swing.aatext", "true");

        MainFrame frame = new MainFrame(args);
        // Validate frames that have preset sizes
        // Pack frames that have useful preferred size info, e.g. from their
        // layout
        if (packFrame) {
            frame.pack();
        } else {
            frame.validate();
        }
    }

    // Main method
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Main(args);
    }
}
