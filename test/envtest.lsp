(defun pwltest ()
  (define-env 'bar (make-big-envelope)))

(defun make-big-envelope ()
  (let (tim val lis (n 500))
    (dotimes (i n)
      (setf tim (* (1+ i) 0.01))
      (setf val (rrandom))
      (setf lis (cons val (cons tim lis))))
    (setf lis (cons (* (1+ n) 0.01) lis))
    (cons 'pwl (reverse lis))))

;(print (make-big-envelope))

(pwltest)

(print "F1 through F9 run through variations of envelopes")
(print "F10 cycles through pwl*, pwe*, pwz* and pwz* with")
(print "    linear attack variants")
(print "F11 changes bias from 0.01 to 0.5")
(print "Full test would be F1 through F9 for each of 4 variants")
(print "    plus change bias on pwz with and without linear attack")
(print "    for a total of 8 * 6 = 48 tests.")
(print "You have to evaluate the plotted output by eye.")
(print "Does not test handling of invalid arguments.")

(setf pwl-test t)
(setf pwe-test nil)
(setf linatk nil)
(setf *bias* 0.01)
  
(defun f2 ()
  (if pwl-test
      (s-plot (pwl .01 1 .2 0.9 .3))
      (if pwe-test
          (s-plot (pwe .01 1 .2 0.9 .3))
          (s-plot (pwz .01 1 .2 0.9 .3 
                       :linear-attack linatk :bias *bias*)))))

(defun f3 () 
  (if pwl-test
      (s-plot (pwl-list '(.02 1 .2 0.9 .3)))
      (if pwe-test
          (s-plot (pwe-list '(.02 1 .2 0.9 .3)))
          (s-plot (pwz-list '(.02 1 .2 0.9 .3)
                            :linear-attack linatk :bias *bias*)))))

(defun f4 ()
  (if pwl-test
      (s-plot (pwlr 0.03 1 .17 0.9 .1))
      (if pwe-test
          (s-plot (pwer 0.03 1 .17 0.9 .1))
          (s-plot (pwzr 0.03 1 .17 0.9 .1 
                        :linear-attack linatk :bias *bias*)))))

(defun f5 ()
  (if pwl-test
      (s-plot (pwlr-list '(0.04 1 .16 0.9 .1)))
      (if pwe-test
          (s-plot (pwer-list '(0.04 1 .16 0.9 .1)))
          (s-plot (pwzr-list '(0.04 1 .16 0.9 .1)
                             :linear-attack linatk :bias *bias*)))))

(defun f6 ()
  (if pwl-test
      (s-plot (pwlv 0 .05 1 .2 0.9 .3 0))
      (if pwe-test
          (s-plot (pwev 0.1 .05 1 .2 0.9 .3 0.1))
          (s-plot (pwzv 0 .05 1 .2 0.9 .3 0 
                        :linear-attack linatk :bias *bias*)))))

(defun f7 ()
  (display "f7")
  (if pwl-test
      (s-plot (pwlv-list '(0 .07 1 .2 0.9 .3 0)))
      (if pwe-test
          (display "pwev-list"
                   (s-plot (pwev-list '(.1 .07 1 .2 0.9 .3 .1))))
          (s-plot (pwzv-list '(0 .07 1 .2 0.9 .3 0)
                             :linear-attack linatk :bias *bias*)))))

(defun f8 () 
  (display "f8")
  (if pwl-test
      (s-plot (pwlvr 0 0.08 1 0.12 0.9 .1 0))
      (if pwe-test
          (s-plot (pwevr .1 0.08 1 0.12 0.9 .1 .1))
          (s-plot (pwzvr 0 0.08 1 0.12 0.9 .1 0
                         :linear-attack linatk :bias *bias*)))))

(defun f9 ()
  (display "f9")
  (if pwl-test
      (s-plot (pwlvr-list '(0 0.09 1 0.11 0.9 .1 0)))
      (if pwe-test
          (s-plot (pwevr-list '(.1 0.09 1 0.11 0.9 .1 .1)))
          (s-plot (pwzvr-list '(0 0.09 1 0.11 0.9 .1 0)
                              :linear-attack linatk :bias *bias*)))))

(defun f10 ()
  (display "f10")
  (if pwl-test
      (setf pwl-test nil pwe-test t)
      (if pwe-test
          (setf pwl-test nil pwe-test nil)
          (if linatk                
              (setf pwl-test t pwe-test nil linatk nil)
              (setf pwl-test nil pwe-test nil linatk t))))
  (display "f10" pwl-test pwe-test linatk *bias*))


(defun f11 ()
  (if (eql *bias* 0.01)
      (setf *bias* 0.5)
      (setf *bias* 0.01))
  (display "f10" pwl-test pwe-test linatk *bias*))

  
