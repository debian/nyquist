;; regression-test.lsp - run lots of stuff
;;
;; Most of these are not actual tests because the output is not tested
;; for correctness, but often these tests reveal bugs when Nyquist is 
;; configured for release.
;;
;; This file extends the ex-global() tests in examples.lsp and examples.sal
;; by running both and then running the typechecks.sal program which really
;; does have unit tests for many type-error checks.
(trace matching-error-message)

(setf *tracenable* t)
;(trace drum-loop)
;(load "plight/drum.lsp")
;(setf *drum-debug* t)
;(load-props-file (strcat *plight-drum-path* "beats.props"))
;(create-drum-patches)
;(create-patterns)
;(setf dsnd (drum 5 5 480))
;(snd-print-tree (aref dsnd 0))
;(play dsnd)
;(exit)
;; windows user may not have drums installed so test first

(princ "---------- LOADING LONGSOUND.LSP ----------\n")
(display "debug" *default-sound-srate*)
(load "longsound.lsp")

(princ "---------- LOADING GATETEST.LSP ----------\n")
(load "gatetest.lsp")

(princ "---------- LOADING RMSTEST.LSP ----------\n")
(load "rmstest.lsp")

(princ "---------- LOADING CONVOLVE.LSP ----------\n")
(load "convolve.lsp")

(princ "---------- LOADING XMTEST.LSP ----------\n")
(load "xmtest.lsp")

(princ "---------- LOADING TYPECHECK-FNS.LSP ----------\n")
(load "typecheck-fns.lsp")
(princ "---------- LOADING TYPECHECKS.LSP ----------\n")
(display "debug" *default-sound-srate*)
(load "typechecks.lsp")
(setfn error original-error)
(setfn sal-print original-sal-print)
(setfn s-plot original-plot)
(setfn ny:error original-nyerror)

(princ "---------- LOADING BIGFILETEST.LSP ----------\n")
(load "bigfiletest.lsp")

(princ "--------- LOADING PLIGHT/DRUM.LSP ----------\n")
(display "debug" *default-sound-srate*)
(load "plight/drum.lsp")
(if (fboundp 'play-drum-example)
    (play-drum-example))

(print (setdir (current-path)))
(print (setdir "../demos/src"))
(load "examples.lsp")
(ex-all)
(sal-load "examples.sal")
(ex-global)
(print (setdir "../../test"))

(princ "---------- END OF REGRESSION-TEST.LSP ----------\n")
