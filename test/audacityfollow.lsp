;; from Steve: this is follow from audacity Jan 2022

; FOLLOW
(defun audfollow (sound floor risetime falltime lookahead)
  (ny:typecheck (not (soundp sound))
    (ny:error "FOLLOW" 1 '((SOUND) "sound") sound))
  (ny:typecheck (not (numberp floor))
    (ny:error "FOLLOW" 2 '((NUMBER) "floor") floor))
  (ny:typecheck (not (numberp risetime))
    (ny:error "FOLLOW" 3 '((NUMBER) "risetime") risetime))
  (ny:typecheck (not (numberp falltime))
    (ny:error "FOLLOW" 4 '((NUMBER) "stepsize") falltime))
  (ny:typecheck (not (numberp lookahead))
    (ny:error "FOLLOW" 5 '((NUMBER) "lookahead") lookahead))
  ;; use 10000s as "infinite" -- that's about 2^30 samples at 96K
  (setf lookahead (round (* lookahead (snd-srate sound))))
  (extract (/ lookahead (snd-srate sound)) 10000
           (snd-follow sound floor risetime falltime lookahead)))
