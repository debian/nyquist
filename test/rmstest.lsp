;; rmstest.lsp -- simple test of rms
;;
(defun rmstest ()
  (let* ((rr (rms (osc c4)))
         (ss (snd-samples rr 100))
         (s50 (aref ss 50))
         (s51 (aref ss 51))
         (e50 (abs (- s50 0.708376)))
         (e51 (abs (- s51 0.692892)))
         (eps (/ 1.0 32767))) ;; full LSB at 16-bits
    (cond ((> e50 eps)
           (error "rmstest error at sample 50" e50))
          ((> e51 eps)
           (error "rmstest error at sample 51" e51))
          (t
           (print "     -> RMSTEST PASSED")))
    ))

(rmstest)
