;; test add, multiply, osc, lp on big sound
;; Mainly a test to see if sound lengths > 32 bits are handled
(defun longtest ()
  (princ "\n------------------\n")
  (print "testing very long sound computation. please wait.")
  (display "longtest" (* 3600 44100) (snd-play '(osc c4 3600)))
  (let ((big (* 27.1 3600)) expected ntotal)
    (display "computing 27.1 hours of samples" big (/ big 3600))
    (format t "0x7fffffff = ~A~%" #x7fffffff)
    (setf expected (round (* big 44100)))
    (display "samples" expected)
    (setf ntotal
          (snd-play '(let ((s (osc c4 big))) (lp (prod s (sum s s)) 8000.0))))
    (if (/= expected ntotal)
        (error "in longtest, expected samples does not match ntotal"))))

(longtest)
