;; Test piano for stretch and duration

;; get the logical stop time:
;;
(defun pt1 (sus str)
  (let (lst tlst)
    (setf s (sustain sus (seq (stretch str (piano-note 1 c4 100))
                              (progn (setf lst (local-to-global 0))
                                     (piano-note 0.1 c4 100)))))
    (snd-length s ny:all)  ;; force evaluation
    (format t "Logical stop time with  sustain ~A, stretch ~A: ~A~%"
            sus str lst)
    ;; logical stop time should be start + stretch
    (setf tlst (local-to-global str)) ;; "true" correct lst
    (cond ((> (abs (- tlst lst)) 0.000001)
           (format t "ERROR: lst ~A should be ~A~%" lst tlst)))
    ;(play s)
    ))
    

(at 10
  (dolist (sus '(1 0.5 1.5))
    (dolist (str '(1 2 0.1 0.2 0.3 1.1))
      (pt1 sus str))))


;; Jean-Paul's test case:
(setq partition
      '((0 1 (sax c4 (stk-breath-env 2 0.2 0.2)))
        (0 2 (sax c5 (stk-breath-env 2 0.1 0.1))) 
        (6 3 (stretch 0.5 (seqrep (step 4) (piano-note 0.4 (+ C4 step) 80))))))

;; duration should be 6 + 3 * 0.5 * 4 * 0.4 + decay time = 8.4 + ?
;; actual time printed by Nyquist is 8.42993 seconds
;; logical stop time printed by Nyquist is 8.4

(play (seq (timed-seq partition)
           (progn (setf lst (local-to-global 0))
                  (format t "LST ~A~%" lst)
                  (s-rest 0.001))))

