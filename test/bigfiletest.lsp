;; bigfiletest.lsp - test read and write of > 32 bit framecount
;;
(defun bigfiletest ()
  (display "What's the max sample count?" ny:all)
  (princ "Testing one-hour mono 16-bit IO\n")
  (fileiotest "hour.sf" 3600)
  (princ "Testing very long sound IO. please wait.\n")
  ;; multiply by 1 for ulaw. Change this to 2 and remove the
  ;; :mode snd-mode-ulaw parameter below for 16-bit. I ran out of
  ;; space on my linux box, but even 1-byte samples exceeds a
  ;; file and sample count size > 2^32.
  (display "byte count estimate" (round (* 27.1 3600 1 44100)))
  (fileiotest "huge.sf" (* 27.1 3600))
  (princ "    --> PASSED BIGFILETEST\n"))

;; write/read a file, then write a very small file to free disk space
(defun fileiotest (name seconds)
  (let (amp count)
    (setf amp (s-save (scale 0.9 (sine c5 seconds))
                      ny:all name (* 1800 44100) ;; progress: 1/2 hour
		      :mode snd-mode-ulaw
                      :format snd-head-IRCAM))
    (format t "Wrote ~A seconds to ~A, peak value was ~A~%" seconds name amp)
    (format t "Checking for soundfile format~%")
    (sf-info name)
    (format t "Reading ~A:~%" name)
    (setf count (snd-play '(s-read name)))
    (format t "Expected ~A samples; read ~A samples from ~A~%"
            (round (* seconds *sound-srate*)) count name)
    (cond ((/= (round (* seconds *sound-srate*)) count)
           (error "failure reading big file")))
    (setf amp (peak (s-read name :time-offset (- seconds 1)) ny:all))
    (cond ((< amp 0.886)
           (error "Something wrong: peak amplitude < 0.886")))
    (format t "Read last second of ~A, peak value was ~A~%" name amp)
    (s-save (s-rest 0.01) ny:all name)
    (format t "Replaced big file ~A with a tiny one.~%" name)
    ))

(bigfiletest)
