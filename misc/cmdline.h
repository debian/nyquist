boolean cl_init(char *switches[], int nsw, char *options[], int nopt,
                char *av[], int ac);
char *cl_option(char *name);
char *cl_noption(char *names[], int nnames);
boolean cl_switch(char *name);
char *cl_nswitch(char *names[], int nnames);
char *cl_arg(int n);
