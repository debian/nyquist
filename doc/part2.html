<!DOCTYPE html>
<html><head><title>Introduction and Overview</title>
<link rel="stylesheet" type="text/css" href="nyquiststyle.css">
<link rel="icon" href="nyquist-icon.png" />
<link rel="shortcut icon" href="nyquist-icon.png" />
</head>
<body bgcolor="ffffff">
<a href = "part1.html">Previous Section</a> | <a href = "part3.html">Next Section</a> | <a href = "title.html#toc">Table of Contents</a> | <a href = "indx.html">Index</a> | <a href = "title.html">Title Page</a>
<hr>
<a name = "2"><h2>Introduction and Overview</h2></a>
<p>Nyquist is a language for sound synthesis and music composition.  Unlike score
languages that tend to deal only with events, or signal processing languages
that tend to deal only with signals and synthesis, Nyquist handles both in a
single integrated system.  Nyquist is also flexible and easy to use because it
is based on an interactive Lisp interpreter.</p>

<p>With Nyquist, you can design instruments by combining functions (much as you
would using the orchestra languages of Music V, cmusic, or Csound).  You can
call upon these instruments and generate a sound just by typing a simple
expression.  You can combine simple expressions into complex ones to create
a whole composition.</p>

<p>Nyquist runs under Linux, Apple macOS, Microsoft Windows NT,
2000, XP, and Vista, 
and it produces sound files or directly generates audio.  
Recent versions have also run on AIX, NeXT, SGI, DEC pmax, and Sun Sparc
machines. (Makefiles for many of these are included, but out-of-date).
Let me know if you have problems with 
any of these machines.</p>

<p>The core language of Nyquist is Lisp. Nyquist extends Lisp for
sound synthesis and processing.
Starting with Version 3, Nyquist supports a variant of
SAL syntax. SAL is a language distinct from Lisp, but it can be
translated to Lisp and SAL and Lisp programs can be combined.
SAL was introduced in Rick
Taube's Common Music system. Since there are some differences between
Nyquist and Common Music, one should generally call this
implementation &ldquo;Nyquist SAL;&rdquo; however, in this manual, I will just
call it &ldquo;SAL.&rdquo; SAL offers most of the capabilities of Lisp, but it
uses an Algol-like syntax that may be more familiar to programmers
with experience in Java, C, Basic, etc. An introduction to SAL is in
Mary Simoni and Roger B. Dannenberg, <i>Algorithmic Composition: A
Guide to Composing Music with Nyquist</i>  <a href = "http://www.algocompbook.com/">(Simoni and Dannenberg 2013)</a>.
To learn about Lisp, an
excellent text by Touretzky is recommended  <a href = "musicbib.html#touretzky">(Touretzky 1984)</a>.  Appendix
<a href = "part19.html#224">Appendix 3: XLISP: An Object-oriented Lisp</a> is the reference manual for XLISP, of which Nyquist is
a superset. </p>
<a name = "3"><h3>Installation</h3></a><a name="index5"></a><a name="index6"></a><a name="index7"></a>
<p>Nyquist is a C++ program intended to run under various operating systems 
including Unix, macOS, and Windows. Nyquist is based on Lisp, but it 
includes its own Lisp interpreter (a modified version of XLISP), so you 
do not need to install some other Lisp to run Nyquist.  Other 
Lisp systems are not compatible with Nyquist.</p>

<p>Most Nyquist users run Nyquist under the Nyquist IDE, which is written in Java 
and depends on the Java runtime system. Most systems already have Java, but if
you do not, you will need to install it. Java from Oracle is recommended, e.g.
Java SE Development Kit 17. When you install the Nyquist IDE,
you will automatically get Nyquist and a set of runtime libraries.</p>

<p>There are generally two ways to install Nyquist:
<ul>
<li>
Get a pre-compiled version of the Nyquist IDE for Windows or macOS. The 
Windows version comes packaged in an installer that installs and 
configures the Nyquist IDE. The macOS version 
unpacks to a complete OS X application.</li>
<li>Compile from sources. There is one set of sources for Mac, Windows, and Unix.
Instructions for building applications from the sources are provided in
the files <code>sys/win/README.txt</code>, <code>sys/mac/README.txt</code>, and 
<code>sys/unix/README.txt</code>.
</li></ul></p>

<p>You can download source code and precompiled versions from the Nyquist project
on SourceForge (<code>http://sourceforge.net/projects/nyquist</code>). The latest 
source code can be obtained via Subversion (svn) using the following:
</p>
<pre>
svn co svn://svn.code.sf.net/p/nyquist/code/trunk/nyquist nyquist
</pre>

<p>
or by checking out nyquist using a graphical interface svn client such as 
TortoiseSVN for Windows.</p>
<a name = "4"><h4>Troubleshooting</h4></a>
<p>All versions of Nyquist includes a Readme.txt file in the top-level
<code>nyquist</code> directory with general information.
Additional information and instructions on installation
can be found in: <ul>
<li> <tt>nyquist/doc/readme-win.txt</tt></li>
<li><tt>nyquist/doc/readme-mac.txt</tt></li>
<li><tt>nyquist/doc/readme-linux.txt</tt>
</li></ul></p>
<a name = "5"><h3>Using NyquistIDE</h3></a><a name="index8"></a><a name="index9"></a><a name="index10"></a>
<p>The program named NyquistIDE is an &ldquo;integrated development
environment&rdquo; for Nyquist. When you run NyquistIDE, it starts the
Nyquist program and displays all Nyquist output in a
window. NyquistIDE helps you by providing a Lisp and SAL editor, hints
for command completion and function parameters, some graphical
interfaces for editing envelopes and graphical equalizers, and a panel
of buttons for common operations. A more complete description of
NyquistIDE is in Chapter <a href = "part3.html#18">The NyquistIDE Program</a>.</p>

<p>For now, all you really need to know is that you can enter Nyquist
commands by typing into the upper left window. When you type return,
the expression you typed is sent to Nyquist, and the results appear in
the window below. You can edit files by clicking on the New File or
Open File buttons. After editing some text, you can load the text into
Nyquist by clicking the Load button. NyquistIDE always saves the file
first; then it tells Nyquist to load the file. You will be prompted
for a file name the first time you load a new file.</p>
<a name = "6"><h3>Using SAL</h3></a>
<p>SAL mode means that Nyquist reads and evaluates SAL commands rather
than Lisp. The SAL mode prompt is "<code>SAL&gt; </code>" while the Lisp mode
prompt is "<code>&gt; </code>".  When Nyquist starts it normally enters SAL
mode automatically, but certain errors may exit SAL mode. You can
reenter SAL mode by typing the Lisp expression <code>(sal)</code>.</p>

<p>In SAL mode, you type commands in the SAL programming
language. Nyquist reads the commands, compiles them into Lisp, and
evaluates the commands. Commands can be entered manually by typing
into the upper left text box in NyquistIDE.</p>
<a name = "7"><h3>Command Line</h3></a><a name="index11"></a><a name="index12"></a><a name="index13"></a>
<p>When run from the command line, Nyquist (usually the command is <tt>ny</tt>) looks 
for some options and some files to load. There are several options.</p>

<p>The <a name="index14"></a><a name="index15"></a><tt>-R</tt><i>paths</i>
option gives Nyquist a list of paths (separated by semicolons) to
directories where Nyquist is allowed to read files. Files in any
subdirectories of the listed directories can also be read, along with
the directories themselves. This option can be used to restrict access
to files.</p>

<p>The <tt>-W</tt><i>paths</i> option gives Nyquist a list of paths (separated by
semicolons) to directories where Nyquist is allowed to write files
(including sound files).  If <tt>-W</tt> is used, Nyquist will not open
audio output to play sounds and will not allow access to Open Sound
Control. This option can be used to protect files from accidentally
malicious code. (If you are truly worried about Nyquist code, you
should run Nyquist in a virtual machine or other isolated machine. It
is unlikely that Nyquist is secure from all attacks.)</p>
<a name="index16"></a><a name="index17"></a>The
<tt>-L</tt><i>run-time-limit</i> option gives Nyquist a run time limit. If the
limit is exceeded, Nyquist prints an error message and exits. The time
is based on Lisp expression evaluations rather than CPU seconds or 
wall time or even execution time involved in printing or signal processing.
One unit
of run time is on the order of 10ms of CPU time (for a medium speed
computer in 2019), but depending on what you are computing, a unit
of run time can vary from 60ms to 1.5ms.
You can use the <code>GET-RUN-TIME</code> function to
read the internal run-time counter to see how much computation you
have done in the same units.<a name="index18"></a><a name="index19"></a><a name="index20"></a>The
<tt>-M</tt><i>memory-limit</i> option gives Nyquist a memory limit. If the
limit (in megabytes) is exceeded, Nyquist prints an error message and
exits. The limit includes all XLISP data (cons cells, strings, etc.)
plus sample blocks and tables allocated for signal processing.
<p>The <tt>-T</tt><i>transcript-file-name</i> option copies console output to a transcript
file.</p>

<p>The <tt>-V</tt> option sets verbose mode. If set, any files specified on
the command line to be loaded are loaded with the verbose option.</p>

<p>Additionally, the command line can name files to be loaded. Nyquist begins by 
loading <tt>init.lsp</tt>, which by default is in the <tt>nyquist/runtime</tt> directory.
This file loads all the standard Nyquist functions. After that, Nyquist will
load any other files named on the command line.</p>

<p>Finally, Nyquist expects to find search paths in the <tt>XLISPPATH</tt> environment
variable (or for Windows, see installation instructions; the path is saved
in the Windows Registry). Normally, you must set <tt>XLISPPATH</tt> for Nyquist to
find <tt>init.lsp</tt> and the definitions of most Nyquist functions.
Be sure you have <tt>system.lsp</tt> in <tt>nyquist/runtime</tt>. Part of the installation
process copies the appropriate <tt>system.lsp</tt> from <tt>nyquist/sys/*</tt> to 
<tt>nyquist/runtime</tt> where it can be found and loaded by <tt>init.lsp</tt>.</p>
<a name = "8"><h3>Helpful Hints</h3></a>
<p>Under Win95 and Win98, the console sometimes locks up. Activating
another window and then reactivating the Nyquist window should unlock
the output.  (We suggest you use NyquistIDE, the interactive
development environment rather than a console window.)</p>

<p>You can cut and paste text into Nyquist, but for serious work, you
will want to use the Lisp <code>load</code> command. To save even more time,
write a function to load your working file, e.g. <code>(defun l ()
(load "myfile.lsp"))</code>. Then you can type <code>(l)</code> to (re)load your
file.</p>

<p>Using SAL, you can type 
</p>
<pre>
define function l () load "myfile.lsp"
</pre>

<p>
and then 
</p>
<pre>
exec l()
</pre>

<p>
to (re)load the file.</p>

<p>The Emacs editor is free GNU software and will help you balance
parentheses if you use Lisp mode. In fact, you can run nyquist
(without the IDE) as a subprocess to Emacs. A helful discussion is
at //http://www.audacity-forum.de/download/edgar/nyquist/nyquist-doc/examples/emacs/main.html. If
you use Emacs, there is also a SAL mode (the file is
<code>sal-mode.el</code>) included with the Common Music distribution, which
you can find on the Web at <code>sourceforge.net</code>.</p>

<p>The NyquistIDE also runs Nyquist as a subprocess and has built-in Lisp
and SAL editors. If your editor does not help you balance parentheses,
you may find yourself counting parens and searching for unbalanced
expressions. If you are confused or desperate and using Lisp syntax,
try the <code>:print t</code> option of the <code>load</code> function. By looking
at the expressions printed, you should be able to tell where the last
unbalanced expression starts.  Alternatively, type
<code>(file-sexprs)</code> and type the lisp file name at the prompt. This
function will read and print expressions from the file, reporting an
error when an extra paren or end-of-file is reached unexpectedly.</p>
<a name = "9"><h3>Using Lisp</h3></a>
<p>Lisp mode means that Nyquist reads and evaluates Nyquist expressions in 
Lisp syntax. Since Nyquist is build on a Lisp interpreter, this is the
&ldquo;native&rdquo; or machine language of Nyquist, and certain errors and functions 
may break out of the SAL interpreter, leaving you with a prompt for a Lisp
expression. Alternatively, you can exit SAL simply by typing <code>exit</code> to
get a Lisp prompt (<code>&gt; </code>). Commands can be entered manually by typing
 into the upper left text box in NyquistIDE.</p>
<a name = "10"><h3>Examples</h3></a>
<p>We will begin with some simple Nyquist programs. Throughout the manual,
we will assume SAL mode and give examples in SAL, but it should be
emphasized that all of these examples can be performed using Lisp
syntax. See Section <a href = "part7.html#78">Interoperability of SAL and XLISP</a> on the relationship between
SAL and Lisp.</p>

<p>Detailed explanations of the functions used in these examples will be
presented in later chapters, so at this point, you should just read these
examples to get a sense of how Nyquist is used and what it can do.  The
details will come later.  Most of these examples can be found in the
file <code>nyquist/lib/examples.sal</code>. Corresponding Lisp syntax
examples are in the file <code>nyquist/lib/examples.lsp</code>.</p>

<p>Our first example makes and plays a sound:<a name="index21"></a><a name="index22"></a>
</p>
<pre>
<i>;; Making a sound.</i>
play osc(60) ; generate a loud sine wave
</pre>

<p></p>

<p>This example is about the simplest way to create a sound with Nyquist.
The <code>osc</code> function generates a sound using a table-lookup
oscillator.  There are a number of optional parameters, but the
default is to compute a sinusoid with an amplitude of 1.0.  The
parameter <code>60</code> designates a pitch of middle C.  (Pitch
specification will be described in greater detail later.)  The result
of the <code>osc</code> function is a sound.  To hear a sound, you must use
the <code>play</code> command, which plays the file through the machine's
D/A converters.  It also writes a soundfile in case the computation
cannot keep up with real time. You can then (re)play the file by
typing:
</p>
<pre>
exec r()
</pre>

<p>
This <code>(r)</code> function is a general way to &ldquo;replay&rdquo; the last thing written by <code>play</code>.</p>

<p>Note: when Nyquist plays a sound, it scales the signal by 
2<sup>15</sup>-1 and (by default)
converts to a 16-bit integer format. A signal like <code>(osc 60)</code>,
which ranges from +1 to -1, will play as a full-scale 16-bit audio
signal.</p>
<a name = "11"><h4>Non-Sinusoidal Waveforms</h4></a>
<p>Our next example will be presented in several steps.  The goal is to create a
sound using a
wavetable consisting of several harmonics as opposed to a simple sinusoid.
In order to build a table, we will use a function that computes a single 
harmonic and add harmonics to form a wavetable.  An special function, 
<code>build-harmonic</code> is used to compute the harmonics, but it is just a
convenient wrapper for the primitive function <code>snd-sine</code>.</p>

<p>The function
<code>mkwave<a name="index23"></a></code> calls upon
<code>build-harmonic<a name="index24"></a></code> to generate a total of four
harmonics with amplitudes 0.5, 0.25, 0.125, and 0.0625.  
These are scaled and added (using <code>+<a name="index25"></a></code>) to
create a waveform which is bound to local variable <code>wave</code>.  </p>

<p>A complete Nyquist wavetable specification
 is a list consisting of a sound, a pitch,
 and <code>T</code>, indicating a periodic waveform.  The pitch gives the
nominal pitch of the sound.  (This is implicit in a single cycle wave
table, but a sampled sound may have many periods of the fundamental.)
Pitch is expressed in half-steps, where middle C is 60 steps, as in MIDI
pitch numbers.
The list of sound, pitch, and <code>T</code> is formed in the last line of
<code>mkwave</code>: since <code>build-harmonic</code> computes signals with a duration
of one second, the fundamental is 1 Hz, and the <code>hz-to-step</code> function
converts to pitch (in units of steps) as required.</p>


<p></p>
<pre>
define function mkwave()
  begin
    with wave = 0.5 * build-harmonic(1, 2048) +
                0.25 * build-harmonic(2, 2048) +
                0.125 * build-harmonic(3, 2048) +
                0.0625 * build-harmonic(4, 2048)
    set *mytable* = list(wave, hz-to-step(1.0), #t)
  end
</pre>

<p></p>

<p>Now that we have defined a function, the next step of this example is
to build the wavetable.  The following code calls <code>mkwave</code> the first
time the code is executed (loaded from a file).  The second time, the
variable <code>*mkwave*</code> will be set, so <code>mkwave</code> will not be
invoked:
</p>
<pre>
if ! boundp(quote(*mkwave*)) then
  begin
    exec mkwave()
    set *mkwave* = #t
  end
</pre>

<p></p>
<a name = "12"><h4>Using Wavetables</h4></a><a name="index26"></a><a name="index27"></a><a name="index28"></a><a name="index29"></a>
<p>When Nyquist starts, several waveforms are created and stored in
global variables for convenience. They are: <code>*sine-table*</code>,
<code>*saw-table*</code>, and <code>*tri-table*</code>, implementing sinusoid,
sawtooth, and triangle waves, respectively. The variable
<code>*table*</code> is initialized to <code>*sine-table*</code>, and it is
<code>*table*</code> that forms the default wavetable for many Nyquist
oscillator behaviors. If you want a proper, band-limited waveform, you
should construct it yourself, but if you do not understand this
sentence and/or you do not mind a bit of aliasing, give
<code>*saw-table*</code> and <code>*tri-table*</code> a try.</p>

<p>Note that in Lisp and SAL, global variables often start and end with
asterisks (*). These are not special syntax, they just happen to be
legal characters for names, and their use is purely a convention.</p>
<a name = "13"><h4>Sequences</h4></a><a name="index30"></a>
<p>Finally, we define <code>my-note<a name="index31"></a></code> to use the waveform,
and play several notes in a simple score. Note that the function
<code>my-note</code> has only one command (a <code>return</code> command), so it
is not necessary to use <code>begin</code> and <code>end</code>. These are only
necessary when the function body consists of a sequence of statements:
</p>
<pre>
define function my-note(pitch, dur)
  return osc(pitch, dur, *mytable*)

play seq(my-note(c4, i), my-note(d4, i), my-note(f4, i), 
         my-note(g4, i), my-note(d4, q))
</pre>

<p></p>

<p>Here, <code>my-note</code> is defined to take pitch and duration as parameters;
it calls <code>osc</code> to do the work of generating a waveform, using
<code>*mytable*</code> as a wave table.  In earlier examples, we saw
<code>osc</code> with only two parameters. In that case, <code>*table*</code> is 
used, but here we specify our new table <code>*mytable*</code>.</p>

<p>The <code>seq</code> function is used to invoke a sequence of behaviors.  Each
note is started at the time the previous note finishes.  The parameters to
<code>my-note</code> are predefined in Nyquist: <code>c4</code> is middle C, <code>i</code> (for
eIghth note) is 0.5, and <code>q</code> (for Quarter note) is 1.0.  See Section
<a href = "#16">Predefined Constants</a> for a complete description.  The result is the sum of
all the computed sounds.</p>

<p>Sequences can also be constructed using the <code>at</code> transformation to 
specify time offsets.</p>
<a name = "14"><h4>Envelopes</h4></a><a name="index32"></a>
<p>The next example will illustrate the use of envelopes.  In Nyquist,
envelopes are just ordinary sounds (although they normally have a low sample
rate).  An envelope<a name="index33"></a> is applied to another sound by
multiplication using the <code>mult<a name="index34"></a></code> function.  The code shows
the definition of <code>env-note<a name="index35"></a></code>, defined in terms of the
<code>note</code> function in the previous example.  In <code>env-note</code>, a 4-phase
envelope is generated using the <code>env<a name="index36"></a></code> function, which is
illustrated in Figure <a href = "#14">1</a>.</p>
<hr>
<blockquote>
</blockquote>
<img src="fig1.gif"><br><br>

<p><b>Figure 1: </b>An envelope generated by the <code>env</code> function.

</p>
<hr>
<p></p>
<pre>
<i>; env-note produces an enveloped note.  The duration
;   defaults to 1.0, but stretch can be used to change
;   the duration.
;   Uses my-note, defined above.
;</i>
define function env-note(p)
  return my-note(p, 1.0) *
         env(0.05, 0.1, 0.5, 1.0, 0.5, 0.4)

<i>; try it out:
;</i>
play env-note(c4)
</pre>

<p></p>

<p>While this example shows a smooth envelope multiplied by an audio signal,
you can also multiply audio signals to achieve
what is often called <i>ring modulation</i><a name="index37"></a>. </p>

<p>In the next example, The <i>stretch</i> operator (<code>~</code>)<a name="index38"></a> 
is used to modify durations:
</p>
<pre>
<i>; now use stretch to play different durations
;</i>
play seq(seq(env-note(c4), env-note(d4)) ~ 0.25,
         seq(env-note(f4), env-note(g4)) ~ 0.5,
         env-note(c4))
</pre>

<p></p>

<p>In addition to <i>stretch</i>, there are a number of transformations supported
 by Nyquist,
and transformations of abstract behaviors is perhaps <i>the</i> fundamental
idea behind Nyquist.  Chapter <a href = "part4.html#27">Behavioral Abstraction</a> is devoted to
explaining this concept, and further elaboration can be found elsewhere
 <a href = "http://www.cs.cmu.edu/~rbd/bib-arctic.html#icmcfugue">(Dannenberg and Frayley 1989)</a>.</p>
<a name = "15"><h4>Piece-wise Linear Functions</h4></a>
<p>It is often convenient to construct signals in Nyquist using a list of
(time, value) breakpoints which are linearly interpolated to form a smooth
signal.  Envelopes created by <code>env</code> are a special case of the more
general piece-wise linear functions created by <code>pwl</code>.  Since <code>pwl</code>
is used in some examples later on, we will take a look at <code>pwl</code>
now.  The <code>pwl</code> function takes a list of parameters which denote (time,
value) pairs.  There is an implicit initial (time, value) pair of (0, 0),
and an implicit final value of 0.  There should always be an odd number of
parameters, since the final value (but not the final time) is implicit. 
Here are some examples:
</p>
<pre>
<i>; symmetric rise to 10 (at time 1) and fall back to 0 (at time 2):
;</i>
pwl(1, 10, 2)
</pre>

<p>
</p>
<pre>
<i>; a square pulse of height 10 and duration 5. 
; Note that the first pair (0, 10) overrides the default initial
; point of (0, 0).  Also, there are two points specified at time 5:
; (5, 10) and (5, 0).  (The last 0 is implicit).  The conflict is
; automatically resolved by pushing the (5, 10) breakpoint back to
; the previous sample, so the actual time will be 5 - 1/sr, where
; sr is the sample rate.
;</i>
pwl(0, 10, 5, 10, 5)
</pre>

<p>
</p>
<pre>
<i>; a constant function with the value zero over the time interval
; 0 to 3.5.  This is a very degenerate form of pwl.  Recall that there
; is an implicit initial point at (0, 0) and a final implicit value of
; 0, so this is really specifying two breakpoints: (0, 0) and (3.5, 0):
;</i>
pwl(3.5)
</pre>

<p>
</p>
<pre>
<i>; a linear ramp from 0 to 10 and duration 1.
; Note the ramp returns to zero at time 1.  As with the square pulse
; above, the breakpoint (1, 10) is pushed back to the previous sample.
;</i>
pwl(1, 10, 1)
</pre>

<p>
</p>
<pre>
<i>; If you really want a linear ramp to reach its final value at the 
; specified time, you need to make a signal that is one sample longer.
; The RAMP function does this:
;</i>
ramp(10) <i>; ramp from 0 to 10 with duration 1 + one sample period
;
; RAMP is based on PWL; it is defined in <b><i>nyquist.lsp</i></b>.
;</i>
</pre>

<p></p>
<a name = "16"><h3>Predefined Constants</h3></a>
<p>For convenience and readability, Nyquist pre-defines some constants, mostly
based on the notation of the Adagio score language, as follows:
<ul>
<li>
<b>Dynamics</b>
Note: these dynamics values are subject to change.
<blockquote>
<tt>lppp<a name="index39"></a></tt> = -12.0 (dB)<br>

<tt>lpp<a name="index40"></a></tt> = -9.0<br>

<tt>lp<a name="index41"></a></tt> = -6.0<br>

<tt>lmp<a name="index42"></a></tt> = -3.0<br>

<tt>lmf<a name="index43"></a></tt> = 3.0<br>

<tt>lf<a name="index44"></a></tt> = 6.0<br>

<tt>lff<a name="index45"></a></tt> = 9.0<br>

<tt>lfff<a name="index46"></a></tt> = 12.0<br>

<tt>dB0<a name="index47"></a></tt> = 1.00<br>

<tt>dB1<a name="index48"></a></tt> = 1.122<br>

<tt>dB10<a name="index49"></a></tt> = 3.1623<br>

</blockquote></li>
<li><b>Durations<a name="index50"></a></b>
<blockquote>
<tt>s<a name="index51"></a></tt> = Sixteenth<a name="index52"></a> = 0.25<br>

<tt>i<a name="index53"></a></tt> = eIghth<a name="index54"></a> = 0.5<br>

<tt>q<a name="index55"></a></tt> = Quarter<a name="index56"></a> = 1.0<br>

<tt>h<a name="index57"></a></tt> = Half<a name="index58"></a> = 2.0<br>

<tt>w<a name="index59"></a></tt> = Whole<a name="index60"></a> = 4.0<br>

<tt>sd<a name="index61"></a>, id<a name="index62"></a>, qd<a name="index63"></a>, hd<a name="index64"></a>, wd<a name="index65"></a></tt> = dotted durations<a name="index66"></a>.<br>

<tt>st<a name="index67"></a>, it<a name="index68"></a>, qt<a name="index69"></a>, ht<a name="index70"></a>, wt<a name="index71"></a></tt> = triplet<a name="index72"></a> durations.<br>

</blockquote></li>
<li><b>Pitches<a name="index73"></a></b> 
<a name="index74"></a><a name="index75"></a><a name="index76"></a>
Pitches are based on an A4 of 440Hz.  To achieve a different tuning,
set <code>*A4-Hertz*</code> to the desired frequency for A4, and call
<code>(set-pitch-names)</code>.  This will recompute the names listed below with a
different tuning.  In all cases, the pitch value 69.0 corresponds exactly to
440Hz, but fractional values are allowed, so for example, if you set
<code>*A4-Hertz*</code> to 444 (Hz), then the symbol <code>A4</code> will be bound to
69.1567, and <code>C4</code> (middle C), which is normally 60.0, will be 60.1567.
<blockquote>
<tt>c0</tt> = 12.0<br>

<tt>cs0, df0</tt> = 13.0<br>

<tt>d0</tt> = 14.0<br>

<tt>ds0, ef0</tt> = 15.0<br>

<tt>e0</tt> = 16.0<br>

<tt>f0</tt> = 17.0<br>

<tt>fs0, gf0</tt> = 18.0<br>

<tt>g0</tt> = 19.0<br>

<tt>gs0, af0</tt> = 20.0<br>

<tt>a0</tt> = 21.0<br>

<tt>as0, bf0</tt> = 22.0<br>

<tt>b0</tt> = 23.0<br>

<tt>c1</tt> ... <tt>b1</tt> = 24.0 ... 35.0<br>

<tt>c2</tt> ... <tt>b2</tt> = 36.0 ... 47.0<br>

<tt>c3</tt> ... <tt>b3</tt> = 48.0 ... 59.0<br>

<tt>c4</tt> ... <tt>b4</tt> = 60.0 ... 71.0<br>

<tt>c5</tt> ... <tt>b5</tt> = 72.0 ... 83.0<br>

<tt>c6</tt> ... <tt>b6</tt> = 84.0 ... 95.0<br>

<tt>c7</tt> ... <tt>b7</tt> = 96.0 ... 107.0<br>

<tt>c8</tt> ... <tt>b8</tt> = 108.0 ... 119.0<br>

</blockquote></li>
<li><b>Miscellaneous</b>
<blockquote>
<code>ny:all</code><a name="index77"></a> = &ldquo;all the samples&rdquo; (i.e. a big number) = 1000000000<br>

</blockquote>
</li></ul></p>
<a name = "17"><h3>More Examples</h3></a>
<p>More examples can be found in the directory <code>demos</code>, part of the standard
Nyquist release. In the Apple OS X version of Nyquist, the
<code>demos</code> and <code>doc</code> directories are inside the NyquistIDE
application bundle. To make it easier to access these hidden files,
the NyquistIDE installs links to these directories the first time you
run it. The links will be in the same directory as the NyquistIDE itself.</p>

<p>The file <code>demos/index.htm</code> is an index to many examples and demonstrations.
Many examples are now Nyquist Extensions which can be downloaded by the
NyquistIDE using the Window : Extension Manager menu item.
Here are some highlights of what is available:
<ul>
<li> Code to create atonal melodies
(<code>nyquist/lib/atonal/atonal-melodies.sal</code>).</li>
<li>How to make arpeggios (<code>nyquist/lib/arpeggiator/arpeggiator.htm</code> and<br>

<code>nyquist/lib/arpeggiator/arp.sal</code>)<a name="index78"></a></li>
<li>Gong sounds by additive synthesis<a name="index79"></a>
(<code>nyquist/lib/pmorales/b1.lsp</code> and<br>

<code>nyquist/lib/pmoralies/mateos/gong.lsp</code>)<a name="index80"></a><a name="index81"></a></li>
<li>Risset's spectral analysis of a chord
(<code>nyquist/lib/pmorales/b2.lsp</code>)<a name="index82"></a><a name="index83"></a></li>
<li>Bell sounds (<code>nyquist/demos/pmorales/b3.lsp</code>,
<code>nyquist/lib/pmorales/e2.lsp</code>,<br>

<code>nyquist/lib/pmorales/partial.lsp</code>, and
<code>nyquist/lib/mateos/bell.lsp</code>)<a name="index84"></a><a name="index85"></a></li>
<li>Drum sounds by Risset (<code>nyquist/lib/pmorales/b8.lsp</code><a name="index86"></a><a name="index87"></a></li>
<li>Shepard tones (<code>nyquist/lib/shepard/shepard.lsp</code> and
<code>nyquist/lib/pmorales/b9.lsp</code>)<a name="index88"></a><a name="index89"></a></li>
<li>Random signals (<code>nyquist/lib/pmorales/c1.lsp</code>)</li>
<li>Buzz with formant filters 
(<code>nyquist/lib/pmorales/buzz.lsp</code>)<a name="index90"></a><a name="index91"></a></li>
<li>Computing samples directly in Lisp (using Karplus-Strong and physical
modelling as examples) (<code>nyquist/lib/pmorales/d1.lsp</code>)<a name="index92"></a><a name="index93"></a><a name="index94"></a><a name="index95"></a><a name="index96"></a><a name="index97"></a></li>
<li>FM Synthesis examples, including FM voices<br>

(<code>nyquist/lib/fm-voices-chowning/fm-voices-chowning.sal</code>) designed by John
Chowning<a name="index98"></a><a name="index99"></a>, tuba sound 
(<code>nyquist/lib/mateos/tuba.lsp</code>)<a name="index100"></a>, and see 
<code>nyquist/lib/pmorales/e2.lsp</code> for examples of FM bell<a name="index101"></a>, wood drum<a name="index102"></a>, brass sounds<a name="index103"></a>, and clarinet sounds.<a name="index104"></a><a name="index105"></a></li>
<li>Rhythmic patterns
(<code>nyquist/lib/rhythm/rhythm_tutorial.html</code>)<a name="index106"></a></li>
<li>Drum Samples and Drum Machine
(<code>nyquist/lib/plight/drum.lsp</code>)<a name="index107"></a><a name="index108"></a><a name="index109"></a>. (See Section <a href = "part16.html#206">Drum Machine</a>).
</li></ul></p>
<hr>
<a href = "part1.html">Previous Section</a> | <a href = "part3.html">Next Section</a> | <a href = "title.html#toc">Table of Contents</a> | <a href = "indx.html">Index</a> | <a href = "title.html">Title Page</a>
</body></html>
