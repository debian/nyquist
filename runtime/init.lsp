; init.lsp -- default Nyquist startup file

(setf *breakenable* t)
(load "nyinit.lsp" :verbose nil)

; add your customizations here:
;    e.g. (setf *default-sf-dir* "...")

; (load "test.lsp")

